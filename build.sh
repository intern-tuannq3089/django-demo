# Stop current container if exists
# docker stop django-demo-container

# Remove image
docker rmi -f tuannq200563/django-demo:latest

# Remove container
# docker rm -f django-demo-container

# Build image
docker build -t tuannq200563/django-demo:latest -f django.dockerfile .

# Run
# docker run --name django-demo-container -p 8000:8000 -d django-demo:latest
